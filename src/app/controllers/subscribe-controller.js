'use strict'

const md5 = require("md5");
const config = require('./../../config/config');
const Validation = require('./../validators/validator');
const mailchimp = require('@mailchimp/mailchimp_marketing');
mailchimp.setConfig({ apiKey: config.apiKey, server: config.server });

const checkSubscriberStatus = async (subscriberHash) => {
  try {
    const response = await mailchimp.lists.getListMember(config.listId, subscriberHash);
    return Promise.resolve(response.status);
  } catch (e) {
    if (e.status === 404) {
      return Promise.resolve(false);
    }
    return Promise.reject(e);
  }
}

exports.subscribe = async (req, res, next) => {
  try {
    let validation = new Validation();
    /* validation field sample, change this validation to adapt to your 
     * mailchimp list fields
     *
     * validation.isRequired(req.body.name, "The name field is required");
     * validation.isRequired(req.body.email, "The email field is required");
     * validation.isRequired(req.body.phone, "The telephone is required");
    */
    if (!validation.isValid()) {
      res.status(400).send(validation.errors()).end();
      return;
    }

    const name = req.body.name;
    const email = req.body.email;
    const phone = req.body.phone;

    const subscriberHash = md5(email.toLocaleLowerCase());

    const subscribeStatus = await checkSubscriberStatus(subscriberHash);
    const optionsList = ['subscribed', 'pending'];
    if (optionsList.includes(subscribeStatus)) {
      return res.status(200).send({ message: 'Parabens! 1' });
    }
    // if subriscriber is or unsudscribed or is not in the list
    const data = {
      email_address: email,
      status: "subscribed",
      merge_fields: {
        FNAME: name,
        PHONE: phone
      }
    };

    let response = {};
    if (!subscribeStatus) {
      response = await mailchimp.lists.addListMember(config.listId, data);
    } else {
      response = await mailchimp.lists.updateListMember(config.listId, subscriberHash, data);
    }

    return res.status(200).send({ message: 'Congratulations!' });
  } catch (e) {
    return res.status(500).send({ message: 'Problem on requisition!' });
  }
}

exports.unsubscribe = async (req, res, next) => {
  try {
    let validation = new Validation();
    validation.isRequired(req.body.email, "O email é campo obrigatório");
    if (!validation.isValid()) {
      res.status(400).send(validation.errors()).end();
      return;
    }

    const email = req.body.email;
    const subscriberHash = md5(email.toLocaleLowerCase());
    await mailchimp.lists.updateListMember(
      config.listId,
      subscriberHash,
      { status: 'unsubscribed' }
    );

    return res.status(200).send({ message: 'Subscription cancel' });
  } catch (e) {
    return res.status(500).send({ message: 'Problem on requisition!' });
  }
}