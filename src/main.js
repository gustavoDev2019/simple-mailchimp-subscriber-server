const express = require('express');
const bodyParser = require('body-parser');

const port = process.env.PORT || 8080;

const subscribeRoute = require('./app/routes/subscribe-route');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/subscribe', subscribeRoute);

app.listen(port);
