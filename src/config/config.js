module.exports = {
  apiKey: process.env.API_KEY,
  server: process.env.SERVER,
  listId: process.env.LIST_ID
}